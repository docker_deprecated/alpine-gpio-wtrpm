# alpine-gpio-wtrpm

#### [alpine-aarch64-gpio-wtrpm](https://hub.docker.com/r/forumi0721alpineaarch64/alpine-aarch64-gpio-wtrpm/)
![Docker Image Version (tag latest semver)](https://img.shields.io/docker/v/forumi0721alpineaarch64/alpine-aarch64-gpio-wtrpm/latest)
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721alpineaarch64/alpine-aarch64-gpio-wtrpm/latest)
![MicroBadger Layers (tag)](https://img.shields.io/microbadger/layers/forumi0721alpineaarch64/alpine-aarch64-gpio-wtrpm/latest)
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpineaarch64/alpine-aarch64-gpio-wtrpm)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpineaarch64/alpine-aarch64-gpio-wtrpm)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : aarch64
* Appplication : [WtRPM](http://www.mupuf.org/blog/2013/05/11/wtrpm-a-web-based-wt-suite-to-power-up-slash-down-your-computers/)
    - A Web-based (Wt) Suite to Power Up/down Your Computers.



----------------------------------------
#### Run

```sh
docker run -d \
           --privileged \
           --device=/dev/mem \
           -p 80:80/tcp \
           -e USER_NAME=<username> \
           -e USER_PASSWD=<password> \
		   -e SBC=(rpi|bpi|opih3) \
           forumi0721alpine[ARCH]/alpine-[ARCH]-gpio-wtrpm:latest
```



----------------------------------------
#### Usage
* URL : [http://localhost:80/](http://localhost:80/)
    - If you want to change config, you need to generate `wt-rpm.json` and  add `-v wt-rpm.json:/etc/wt-rpm.json` to docker option.



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| --privileged       | Need for gpio access                             |
| --device=/dev/mem  | Need for gpio access                             |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| 80/tcp             | HTTP port                                        |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| SBC                | SBC Type (rpi, bpi. opih3)                       |
| USER_NAME          | Login username (default : forumi0721)            |
| USER_PASSWD        | Login password (default : passwd)                |
| USER_EPASSWD       | Login password (base64)                          |

